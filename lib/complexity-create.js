'use strict';

var TYPE = {
    UNKNOWN: "unknown",

    BREAK: "break",
    CATCH: "catch",
    CONDITIONAL: "conditional",
    CONTINUE: "continue",
    DO_WHILE: "do_while",
    FOR: "for",
    FOR_IN: "for_in",
    FOR_OF: "for_of",
    FUNCTION: "function",
    IF: "if",
    IF_ELSE: "if_else",
    LABEL: "label",
    LOGICAL: "logical",
    OBJECT: "object",
    RETURN: "return",
    SWITCH: "switch",
    SWITCH_CASE: "case",
    SWITCH_DEFAULT: "default",
    THROW: "throw",
    TRY: "try",
    WHILE: "while"
};

var DEFAULT_VALUE = {
    unknown: 1,

    "break": 1,
    "catch": 1,
    conditional: 1,
    "continue": 1,
    do_while: 1,
    "for": 1,
    for_in: 1,
    for_of: 1,
    "function": 1,
    "if": 1,
    if_else: 2,
    label: 1,
    logical: 1,
    "object": 1,
    "return": 1,
    "switch": 1,
    "case": 1,
    "default": 1,
    "throw": 1,
    "try": 1,
    "while": 1
};

module.exports = function create(context) {
    //-------------------------------------------------------------------------
    // Process options
    //-------------------------------------------------------------------------

    var THRESHOLD = 20;
    var SHOW_REPORT = false;
    var VALUE = {};

    var options = context.options[0];

    if (typeof options === "object") {
        if (options.hasOwnProperty("maximum") && typeof options.maximum === "number") {
            THRESHOLD = options.maximum;
        }

        if (options.hasOwnProperty("report") && typeof options.report === "boolean") {
            SHOW_REPORT = options.report;
        }

        for (var key in DEFAULT_VALUE) {
            VALUE[key] = (options.hasOwnProperty(key) && typeof options[key] === "number")
            ? options[key]
            : DEFAULT_VALUE[key];
        }
    } else if (typeof option === "number") {
        THRESHOLD = option;
    }

    //--------------------------------------------------------------------------
    // Helpers
    //--------------------------------------------------------------------------

    // Using a stack to store complexity (handling nested functions)
    var complexityReport = [];

    /**
     * When parsing a new function, store it in our function stack
     * @returns {void}
     * @private
     */
    function startFunction(node) {
        complexityReport.push([[TYPE.FUNCTION, VALUE[TYPE.FUNCTION], node]]);
    }

    /**
     * Evaluate the node at the end of function
     * @param {ASTNode} node node to evaluate
     * @returns {void}
     * @private
     */
    function endFunction(node) {
        var report = complexityReport.pop();
        var complexity = getComplexityReport(report);

        // emit notice?
        if (complexity.score > THRESHOLD) {
            emitReport(node, complexity);
        }

        // concat into parent?
        if (complexityReport.length > 0) {
            var latest = complexityReport.pop();
            complexityReport.push(latest.concat(report));
        }
    }

    function getComplexityReport(report) {
        var complexityScore = 0;

        var complexityBreakdown = report.map(function (entry) {
            complexityScore += entry[1];

            return entry[0] + " +" + entry[1];
        }, []).join(", ");

        return {
            score: complexityScore,
            breakdown: complexityBreakdown
        };
    }

    function emitReport(node, complexityReport) {
        var template = "Function '{{methodName}}' has a complexity of {{complexityScore}}.";
        var replacements = {
            methodName: getFunctionName(node),
            complexityScore: complexityReport.score,
            complexityBreakdown: complexityReport.breakdown
        };

        if (SHOW_REPORT) {
            template = "Function '{{methodName}}' has a complexity of {{complexityScore}} ({{complexityBreakdown}}).";
        }

        context.report(node, template, replacements);
    }

    function getFunctionName(node) {
        var name = "anonymous";

        if (node.id) {
            name = node.id.name;
        } else if (node.parent.type === "MethodDefinition" || node.parent.type === "Property") {
            name = node.parent.key.name;
        }

        return name;
    }

    function getLatestComplexity() {
        return complexityReport[complexityReport.length - 1];
    }

    /**
     * Increase the complexity of the function in context
     * @returns {void}
     * @private
     */
    function increaseComplexity(type, node) {
        type = type || TYPE.Unknown;

        const context = getLatestComplexity();
        if (!context) { return; }

        context.push([type, VALUE[type] || 1, node]);
    }

    /**
     * Increase the switch complexity in context
     * @param {ASTNode} node node to evaluate
     * @returns {void}
     * @private
     */
    function increaseSwitchCaseComplexity(node) {
        return (node.test)
        ? increaseComplexity(TYPE.SWITCH_CASE)
        : increaseComplexity(TYPE.SWITCH_DEFAULT);
    }

    /**
     * Attempts to catch else statement and ifs with elses.
     */
    function increaseBlockComplexity(node) {
        if (node.parent.type === "IfStatement") {
            // skip direct if-then blocks
            if (node.parent.consequent !== node) {
                // found an else
                // upgrade sibling & parent ifs to ifElse
                complexityReport.forEach(entry => {
                    if (!entry[2]) { return; }
                    if (entry[2] === node.parent || entry[2].parent !== node.parent) {
                        entry[0] = TYPE.IF_ELSE;
                        entry[1] = VALUE[TYPE.IF_ELSE];
                    }
                })
            }
        }
    }

    /**
     * Increase the logical path complexity in context
     * @param {ASTNode} node node to evaluate
     * @returns {void}
     * @private
     */
    function increaseLogicalComplexity(node) {
        // Avoiding &&
        if (node.operator === "||") {
            increaseComplexity(TYPE.LOGICAL);
        }
    }

    //--------------------------------------------------------------------------
    // Public API
    //--------------------------------------------------------------------------

    return {
        FunctionDeclaration: startFunction,
        FunctionExpression: startFunction,
        ArrowFunctionExpression: startFunction,

        "FunctionDeclaration:exit": endFunction,
        "FunctionExpression:exit": endFunction,
        "ArrowFunctionExpression:exit": endFunction,

        BlockStatement: increaseBlockComplexity,
        LogicalExpression: increaseLogicalComplexity,
        SwitchCase: increaseSwitchCaseComplexity,

        BreakStatement: increaseComplexity.bind(undefined, TYPE.BREAK),
        CatchClause: increaseComplexity.bind(undefined, TYPE.CATCH),
        ConditionalExpression: increaseComplexity.bind(undefined, TYPE.CONDITIONAL),
        ContinueStatement: increaseComplexity.bind(undefined, TYPE.CONTINUE),
        DoWhileStatement: increaseComplexity.bind(undefined, TYPE.DO_WHILE),
        ForStatement: increaseComplexity.bind(undefined, TYPE.FOR),
        ForInStatement: increaseComplexity.bind(undefined, TYPE.FOR_IN),
        ForOfStatement: increaseComplexity.bind(undefined, TYPE.FOR_OF),
        IfStatement: increaseComplexity.bind(undefined, TYPE.IF),
        LabeledStatement: increaseComplexity.bind(undefined, TYPE.LABEL),
        ObjectExpression: increaseComplexity.bind(undefined, TYPE.OBJECT),
        ReturnStatement: increaseComplexity.bind(undefined, TYPE.RETURN),
        SwitchStatement: increaseComplexity.bind(undefined, TYPE.SWITCH),
        TryStatement: increaseComplexity.bind(undefined, TYPE.TRY),
        ThrowStatement: increaseComplexity.bind(undefined, TYPE.THROW),
        WhileStatement: increaseComplexity.bind(undefined, TYPE.WHILE)
    };
};
