'use strict';

module.exports = {
    docs: {
        description: "enforce a maximum cyclomatic complexity allowed in a program",
        category: "Best Practices",
        recommended: false
    },

    schema: [
        {
            oneOf: [
                {
                    type: "integer",
                    minimum: 0
                },
                {
                    type: "object",
                    properties: {
                        maximum: {
                            type: "integer",
                            minimum: 0
                        },

                        report: {
                            type: "boolean",
                            minimum: 0
                        },

                        "function": {
                            type: "number",
                            minimum: 0
                        },
                        "return": {
                            type: "number",
                            minimum: 0
                        },
                        "object": {
                            type: "number",
                            minimum: 0
                        },
                        "if": {
                            type: "number",
                            minimum: 0
                        },
                        if_else: {
                            type: "number",
                            minimum: 0
                        },
                        label: {
                            type: "number",
                            minimum: 0
                        },
                        logical: {
                            type: "number",
                            minimum: 0
                        },
                        "switch": {
                            type: "number",
                            minimum: 0
                        },
                        "case": {
                            type: "number",
                            minimum: 0
                        },
                        "default": {
                            type: "number",
                            minimum: 0
                        },
                        "continue": {
                            type: "number",
                            minimum: 0
                        },
                        "break": {
                            type: "number",
                            minimum: 0
                        },
                        "catch": {
                            type: "number",
                            minimum: 0
                        },
                        "while": {
                            type: "number",
                            minimum: 0
                        },
                        do_while: {
                            type: "number",
                            minimum: 0
                        },
                        "for": {
                            type: "number",
                            minimum: 0
                        },
                        for_in: {
                            type: "number",
                            minimum: 0
                        },
                        for_of: {
                            type: "number",
                            minimum: 0
                        },
                        conditional: {
                            type: "number",
                            minimum: 0
                        },
                        "try": {
                            type: "number",
                            minimum: 0
                        },
                        "throw": {
                            type: "number",
                            minimum: 0
                        }
                    }
                }
            ]
        }
    ]
};
